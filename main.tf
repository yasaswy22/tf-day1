provider "aws" {
      region = "us-east-1"
      access_key = "AKIAW77HYQCVM7UYUDGV"
      secret_key = "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"
}

resource "aws_key_pair" "ec2-access-demo" {
  key_name   = "rsa-key-20221025"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiM1kiRigSwtxhrR2I05hfbDGBPEsdXTPwndePoj18rtIwKVvWNafiRz6zdbdBH9/3e8wU6fXmJQNIx0y+q/ICFEDQ0YTKQQe2BjPlgUZSlbGm7VV+A/pCAfVLbhl4U8zI6r2IEq38IVhpoY+O5PlyDXAzZbrLwQvmH4mHzDlDkkeMxSXMnRgNEGWUG+/sRan566sRqhSnh0DsJ3xsVUMumoOKuQNnZuN/zAljFOgjg8kB5rSOfz80eW9b8BNp5cj0OxNhk/U7UD4a3DKFnrdVjNnk+r8Y1FE0Z+utCeOlSujV1Sxw3sEMq4ma/4nSzno2NcO0tSPGlFgVuhzTOdLR rsa-key-20221025"
}

resource "aws_instance" "ec2-demo" {
  ami           = "ami-09d3b3274b6c5d4aa"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.ec2-access-demo.key_name}"
  vpc_security_group_ids = ["${aws_security_group.sg-demo.id}"]
  tags = {
    Name = "demo-tf"
  }
}

resource "aws_security_group" "sg-demo" {
  name        = "ec2-ssh-access"
  description = "Allow SSH inbound traffic"
  
  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "demo"
  }
}